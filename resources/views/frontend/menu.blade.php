@extends('layouts.app')

@section('body')
<style>
    h1{
        font-family:Roboto
    }
    .menu_link{
        padding:0rem;
        color:#2c2725;
        text-transform:uppercase;
        background:#fbdbcc;
        line-height:105px;
    font-size: 24px;
    border-radius:0px !important;
    }
    .nav-link.active{
      color:#2c2725 !important;

    }
    .menu_div{
        max-width:70%;
    }
    .menu_subhead{
        color:#2c2725;
        font-size:18px;
    }
    .menu_category{
      color: #2c2725;
    font-weight: 600;
    font-size: 24px;
    margin:3rem 0;
        
    }
    .menu_title{
      color:#2c2725;
      font-weight:600;
        font-size:20px; 
    }
    .menu_subtitle{
      color:#2c2725;

        font-size:16px;
    }
    .menu_items{
        margin-bottom:4rem;
    }
</style>
{{-- <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-min-height-100% g-flex-middle">
        <div class="row no-gutters">
          <div class="col-md-6 order-md-1" style="height:100%; background:#070c17;">
            <div class="g-pa-15x">
              <div class="g-mb-100">
                <h1 class="g-color-white g-font-weight-700 g-font-size-50 g-letter-spacing-0_5 mb-4">We are Unify</h1>
                <p class="lead g-color-white">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key.</p>
              </div>
  
              <div class="row">
                <div class="col-md-7 col-lg-6 order-md-2 g-mb-50 g-mb-0--md">
                  <h2 class="h4 g-color-white-opacity-0_9 g-font-weight-700 mb-4">Contact us</h2>
                  <ul class="list-unstyled g-color-white-opacity-0_8 g-font-size-13 mb-0">
                    <li class="mb-3">
                      <i class="mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i> 795 Folsom Ave, Suite 600,
                      <br>San Francisco, CA 94107 795
                    </li>
                    <li class="mb-3"><i class="mt-1 mr-3 icon-communication-033 u-line-icon-pro"></i>(+123) 456 7890
                    </li>
                    <li class="mb-3"><i class="mt-1 mr-3 icon-communication-062 u-line-icon-pro"></i>info@htmlstream.com
                    </li>
                    <li><i class="mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i> www.htmlstream.com</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 order-md-2 g-color-black-opacity-0_8 g-bg-size-cover g-bg-pos-top-center g-min-height-400" data-bg-img-src="/menu1.jpg" style="background-image: url(/menu1.jpg);"></div>
        </div>
</section> --}}

<section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-min-height-100% g-flex-middle" data-options="{direction: 'reverse';, settings_mode_oneelement_max_offset: '150';}">
        <div class="divimage dzsparallaxer--target w-100 g-bg-pos-bottom-center" style="height: 120%; background-image: url(/main4.jpg); transform: translate3d(0px, -85.5069px, 0px);"></div>
  
        <div class="container g-py-100">
          <div class="row">
            <div class="col-md-6">
              <h3 style="font-size:40px" class="g-color-white Restaurant-Display tp-resizeme mb-4">
            THE MENU
          </h3>
              <p class="Restaurant-Price" style="text-align: justify;font-size:18px; line-height:30px">The Delhi ‘O’ Delhi brand is now well-established as an an event service partner in and around Sydney, including affiliations with five-star hotels in CBD and the most well- established function centres. Repeat business with out esteemed partners from alarge portion of our catering services.
<br><br>
                    ” We now cater for weddings, family, event and all kind of social occasions and we are also increasingly asked to cater for large corporate events. The Delhi ‘O’ Delhi brand has grown more quickly that I ever anticipated. “</p>
              
            </div>
          </div>
        </div>
</section>

<section>
<ul class="nav nav-justified u-nav-v2-1 u-nav-primary u-nav-rounded-5" role="tablist" data-target="nav-2-1-primary-hor-justified" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block u-btn-outline-primary g-mb-20">
  <li class="nav-item">
    <a class="nav-link Restaurant-Price menu_link active" data-toggle="tab" href="#nav-2-1-primary-hor-justified--1" role="tab">Dinner Menu</a>
  </li>
  {{-- <li class="nav-item">
    <a class="nav-link Restaurant-Price menu_link" data-toggle="tab" href="#nav-2-1-primary-hor-justified--2" role="tab">Poultry</a>
  </li> --}}
  <li class="nav-item">
    <a class="nav-link Restaurant-Price menu_link" data-toggle="tab" href="#nav-2-1-primary-hor-justified--3" role="tab">Drinks</a>
  </li>
</ul>
<!-- End Nav tabs -->

<!-- Tab panes -->
<div >
  <div id="nav-2-1-primary-hor-justified" class="tab-content g-pb-10" style="background: url(/leavesbg.jpg);background-size: contain;" >
      <div class="tab-pane fade show active  " id="nav-2-1-primary-hor-justified--1" role="tabpanel">
          <div class="g-pt-40 d-flex justify-content-center">
              <div class="menu_div text-center">
              
                  <p class="Restaurant-Price menu_subhead">The main course menu welcomes guests to delve further into our reimagination.</p>
                  <h3 class="Restaurant-Price menu_category">10 Course</h3>
              
                  <div class="menu_items">
                      <p class="Restaurant-Cursive menu_title">SHAAN E RAAN 80</p>
                      <p class="Restaurant-Price menu_subtitle">Okra tossed with ginger, red onions, cumin & coriander. </p>
                  </div>
                  <div class="menu_items">

                      <p class="Restaurant-Cursive menu_title">BHINDI DO PYAAZA [v] 19</p>
                      <p class="Restaurant-Price menu_subtitle">Succulent whole leg of lamb (2.5Kgs) infused with cinnamon oil and marinated with potli spices for 24 hours and cooked to perfection in our clay oven </p>
                  </div>
                  <div class="menu_items">

                      <p class="Restaurant-Cursive menu_title">DATES & APRICOT KOFTA [n] 21</p>
                      <p class="Restaurant-Price menu_subtitle">Potatoes & cheese dumplings stuffed with dates & apricot with kaffir lime sauce & almonds. </p>
                  </div>
                  <div class="menu_items">
                      <p class="Restaurant-Cursive menu_title">PALAK PANEER 21</p>
                      <p class="Restaurant-Price menu_subtitle">Spinach and cottage cheese sautéed with cracked cumin, garlic, roasted red chili and tomatoes. </p>
                  </div>
                  <div class="menu_items">

                      <p class="Restaurant-Cursive menu_title">DAL MAKHNI 16</p>
                      <p class="Restaurant-Price menu_subtitle">Aslowly cooked creamy velvety stew of black lentils and fresh ginger.</p>
                  </div>
                  <div class="menu_items">

                      <p class="Restaurant-Cursive menu_title">GOBI BAINGAN ACHARI [v] 19</p>
                      <p class="Restaurant-Price menu_subtitle">Cauliflower florets & eggplant cooked in pickled gravy.</p>
                  </div>
                  <div class="menu_items">

                      <p class="Restaurant-Cursive menu_title">GOBI BAINGAN ACHARI [v] 19</p>
                      <p class="Restaurant-Price menu_subtitle">Cauliflower florets & eggplant cooked in pickled gravy.</p>
                  </div>
                  <div class="menu_items">

                      <p class="Restaurant-Cursive menu_title">GOBI BAINGAN ACHARI [v] 19</p>
                      <p class="Restaurant-Price menu_subtitle">Cauliflower florets & eggplant cooked in pickled gravy.</p>
                  </div>
                  
              </div>
          </div>
      </div>

      {{-- <div class="tab-pane fade " id="nav-2-1-primary-hor-justified--2" role="tabpanel">
          <div class="g-pt-40 d-flex justify-content-center " >
        
              <div class="menu_div text-center">
              
                      <p class="Restaurant-Price menu_subhead">The poultry menu welcomes guests to delve further into our reimagination.</p>
                      
                      <h3 class="Restaurant-Price menu_category">10 Course</h3>

                      <div class="menu_items">
                          <p class="Restaurant-Cursive menu_title">SHAAN E RAAN 80</p>
                          <p class="Restaurant-Price menu_subtitle">Okra tossed with ginger, red onions, cumin & coriander. </p>
                      </div>
                      <div class="menu_items">
          
                          <p class="Restaurant-Cursive menu_title">BHINDI DO PYAAZA [v] 19</p>
                          <p class="Restaurant-Price menu_subtitle">Succulent whole leg of lamb (2.5Kgs) infused with cinnamon oil and marinated with potli spices for 24 hours and cooked to perfection in our clay oven </p>
                      </div>
                      <div class="menu_items">
          
                          <p class="Restaurant-Cursive menu_title">DATES & APRICOT KOFTA [n] 21</p>
                          <p class="Restaurant-Price menu_subtitle">Potatoes & cheese dumplings stuffed with dates & apricot with kaffir lime sauce & almonds. </p>
                      </div>
                      <div class="menu_items">
                          <p class="Restaurant-Cursive menu_title">PALAK PANEER 21</p>
                          <p class="Restaurant-Price menu_subtitle">Spinach and cottage cheese sautéed with cracked cumin, garlic, roasted red chili and tomatoes. </p>
                      </div>
                      <div class="menu_items">
          
                          <p class="Restaurant-Cursive menu_title">DAL MAKHNI 16</p>
                          <p class="Restaurant-Price menu_subtitle">Aslowly cooked creamy velvety stew of black lentils and fresh ginger.</p>
                      </div>
                      <div class="menu_items">
          
                          <p class="Restaurant-Cursive menu_title">GOBI BAINGAN ACHARI [v] 19</p>
                          <p class="Restaurant-Price menu_subtitle">Cauliflower florets & eggplant cooked in pickled gravy.</p>
                      </div>
                      <div class="menu_items">
          
                        <p class="Restaurant-Cursive menu_title">DATES & APRICOT KOFTA [n] 21</p>
                        <p class="Restaurant-Price menu_subtitle">Potatoes & cheese dumplings stuffed with dates & apricot with kaffir lime sauce & almonds. </p>
                    </div>
                    <div class="menu_items">
                        <p class="Restaurant-Cursive menu_title">PALAK PANEER 21</p>
                        <p class="Restaurant-Price menu_subtitle">Spinach and cottage cheese sautéed with cracked cumin, garlic, roasted red chili and tomatoes. </p>
                    </div>
                    <div class="menu_items">
        
                        <p class="Restaurant-Cursive menu_title">DAL MAKHNI 16</p>
                        <p class="Restaurant-Price menu_subtitle">Aslowly cooked creamy velvety stew of black lentils and fresh ginger.</p>
                    </div>
                    <div class="menu_items">
        
                        <p class="Restaurant-Cursive menu_title">GOBI BAINGAN ACHARI [v] 19</p>
                        <p class="Restaurant-Price menu_subtitle">Cauliflower florets & eggplant cooked in pickled gravy.</p>
                    </div>
                      
              </div>
          </div>
      </div> --}}
      <div class="tab-pane fade" id="nav-2-1-primary-hor-justified--3" role="tabpanel">
          <div class="g-pt-40 d-flex justify-content-center">
          
              <div class="menu_div text-center">
                  
                      <p class="Restaurant-Price menu_subhead">With an emphasis on showcasing quality wines that communicate a sense of place and time, the Quay wine list champions locally sourced produce while still tipping its cap to international styles. A list of ten cocktails and ten non-alcoholic cocktails has been crafted to complement the wine list.</p>
                      
                      <h3 class="Restaurant-Price menu_category">10 Course</h3>
                    
                      <div class="menu_items">
                          <p class="Restaurant-Cursive menu_title">SHAAN E RAAN 80</p>
                          <p class="Restaurant-Price menu_subtitle">Okra tossed with ginger, red onions, cumin & coriander. </p>
                      </div>
                      <div class="menu_items">

                          <p class="Restaurant-Cursive menu_title">BHINDI DO PYAAZA [v] 19</p>
                          <p class="Restaurant-Price menu_subtitle">Succulent whole leg of lamb (2.5Kgs) infused with cinnamon oil and marinated with potli spices for 24 hours and cooked to perfection in our clay oven </p>
                      </div>
                      <div class="menu_items">

                          <p class="Restaurant-Cursive menu_title">DATES & APRICOT KOFTA [n] 21</p>
                          <p class="Restaurant-Price menu_subtitle">Potatoes & cheese dumplings stuffed with dates & apricot with kaffir lime sauce & almonds. </p>
                      </div>
                      <div class="menu_items">
                          <p class="Restaurant-Cursive menu_title">PALAK PANEER 21</p>
                          <p class="Restaurant-Price menu_subtitle">Spinach and cottage cheese sautéed with cracked cumin, garlic, roasted red chili and tomatoes. </p>
                      </div>
                      <div class="menu_items">

                          <p class="Restaurant-Cursive menu_title">DAL MAKHNI 16</p>
                          <p class="Restaurant-Price menu_subtitle">Aslowly cooked creamy velvety stew of black lentils and fresh ginger.</p>
                      </div>
                      <div class="menu_items">

                          <p class="Restaurant-Cursive menu_title">GOBI BAINGAN ACHARI [v] 19</p>
                          <p class="Restaurant-Price menu_subtitle">Cauliflower florets & eggplant cooked in pickled gravy.</p>
                      </div>
                      
              </div>
          </div>
      
      
      </div>
  </div>
</div>
</section>

@endsection


@section('js')
<script  src="/frontend-assets/main-assets/assets/js/components/hs.tabs.js"></script>

<!-- JS Plugins Init. -->
<script >
  $(document).on('ready', function () {
    // initialization of tabs
    $.HSCore.components.HSTabs.init('[role="tablist"]');
  });

  $(window).on('resize', function () {
    setTimeout(function () {
      $.HSCore.components.HSTabs.init('[role="tablist"]');
    }, 200);
  });
</script>
@endsection