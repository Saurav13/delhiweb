@extends('layouts.app')

@section('body')
<style>
        h1{
            font-family:Roboto
        }
        .menu_link{
            padding:0rem;
            text-transform:uppercase;
            background:#2b4e56;
            line-height:105px;
        font-size: 24px;
        color:#2c2725 !important;
         
        border-radius:0px !important;
        }

        .menu_div{

            max-width:70%;
        }
        .menu_subhead{
        color:#2c2725 !important;

            color:#2c2725 !important;

            font-size:18px;
        }
        .menu_title{
        color:#2c2725 !important;

            color:#2c2725 !important;

            font-size:20px; 
        }
        .menu_subtitle{
            color:#2c2725 !important;
            font-size:16px;
        }
        .menu_items{
            margin-bottom:4rem;
        }
        .p_description{
            color:#2c2725 !important;
            text-align: justify;
            font-size:18px; 
            line-height:30px
        }
        .btn_link{
            z-index: 19;
        white-space: nowrap;
        color:#2c2725 !important; 
        font-size: 17px;
        line-height: 17px;
        font-weight: 500;
        font-family: Roboto;
        text-transform: uppercase;
        background-color: rgba(10, 10, 10, 0);
        border: 2px solid #2c2725;
        letter-spacing: 3px;
        
        }
        .btn_link:hover{
            background:#fbdbcc;

            border-color: #2b4e56;
        }
        .btn_lg{
            /* font-size: 22px; */
            padding: 1rem 4rem;
        }
        .event_date_time{
            font-size: 18px;
        color: #2c2725 !important;
        }
</style>

<section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-min-height-100% g-flex-middle">
    <div class="row no-gutters">
        <div class="col-md-6 order-md-2 g-bg-primary" style="height:100%;">
        <div style="padding: 10% 10%">
            <div class="g-mb-20">
            <h3 style="font-size:40px;line-height:105px;color:#2c2725 !important; " class="g-color-white Restaurant-Cursive tp-resizeme mb-4">Event Title</h3>
            <p class="Restaurant-Price p_description ">
                    The Delhi ‘O’ Delhi brand is now well-established as an event services partner in and around Sydney, including affiliations with five-star hotels in CBD and the most well-established function centers. Repeat business with our esteemed partners from a large portion of our catering services.

            </p>
            <p style="margin-top:3rem;">
                <span class="Restaurant-Price event_date_time" >Date: </span> <span  class="Restaurant-Price event_date_time">13th July</span>
                <br>
                <span class="Restaurant-Price event_date_time" >Time: </span>: <span class="Restaurant-Price event_date_time">2:00 PM</span>
                <br>
                <span class="Restaurant-Price event_date_time" >Price: </span>: <span  class="Restaurant-Price event_date_time">$40</span>

            </p>
            <br>
            <button style="margin-right:1rem" class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link">Veg Menu</button>
            <button class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link">Non-veg Menu</button>
            <br><br>
            <button class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link btn_lg">Make a Reservation</button>

        </div>

            
        </div>
        </div>
        <div class="col-md-6 order-md-1 g-color-black-opacity-0_8 g-bg-size-cover g-bg-pos-top-center g-min-height-400" data-bg-img-src="/event1.jpg" style="background-image: url(/event1.jpg);"></div>
    </div>

    <div class="row no-gutters">
        <div class="col-md-6 order-md-1 g-bg-primary" style="height:100%;">
        <div style="padding: 10% 10%">
            <div class="g-mb-20">
        <h3 style="font-size:40px;line-height:105px;color:#2c2725 !important; " class=" Restaurant-Cursive tp-resizeme mb-4">Event Title</h3>
            <p class="Restaurant-Price p_description ">
                    The Delhi ‘O’ Delhi brand is now well-established as an event services partner in and around Sydney, including affiliations with five-star hotels in CBD and the most well-established function centers. Repeat business with our esteemed partners from a large portion of our catering services.

            </p>
            <p style="margin-top:3rem;">
                <span class="Restaurant-Price event_date_time" >Date: </span> <span class="Restaurant-Price event_date_time">13th July</span>
                <br>
                <span class="Restaurant-Price event_date_time" >Time: </span> <span  class="Restaurant-Price event_date_time">2:00 PM</span>
                <br>
                <span class="Restaurant-Price event_date_time" >Price: </span> <span  class="Restaurant-Price event_date_time">$40</span>

            </p>
            <br>
            <button style="margin-right:1rem" class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link">Veg Menu</button>
            <button class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link">Non-veg Menu</button>
            <br><br>
            <button class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link btn_lg">Make a Reservation</button>

        </div>

            
        </div>
        </div>
        <div class="col-md-6 order-md-2 g-color-black-opacity-0_8 g-bg-size-cover g-bg-pos-top-center g-min-height-400" data-bg-img-src="/event2.jpg" style="background-image: url(/event2.jpg);"></div>
    </div>


</section>
@endsection