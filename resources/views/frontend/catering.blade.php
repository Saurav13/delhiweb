@extends('layouts.app')

@section('body')
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
  <link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.css">
                  
<style>
    h1{
        font-family:Roboto
    }
    .menu_link{
        padding:0rem;
        text-transform:uppercase;
        background:#2b4e56;
        line-height:105px;
        color:#2c2725 !important;

        font-size: 24px;
        border-radius:0px !important;
    }
    .menu_div{
        max-width:70%;
    }
    .menu_subhead{
        color:#2c2725 !important;

        color:white;
        font-size:18px;
    }
    .menu_title{
        color:#2c2725 !important;

        font-size:20px; 
    }
    .menu_subtitle{
        color:#2c2725 !important;

        font-size:16px;
    }
    .menu_items{
        color:#2c2725 !important;

        margin-bottom:4rem;
    }
    .p_description{
        color:#2c2725 !important;

        text-align: justify;
        font-size:18px; 
        line-height:30px
    }
    .btn_link{
        z-index: 19;
        color:#2c2725 !important;
    white-space: nowrap;
    font-size: 17px;
    line-height: 17px;
    font-weight: 500;
    color: rgb(255, 255, 255);
    font-family: Roboto;
    text-transform: uppercase;
    background-color: rgba(10, 10, 10, 0);
    border: 2px solid #2c2725;
    letter-spacing: 3px;
    }
    .btn_link:hover{
      background:#fbdbcc;
        border-color: #2b4e56;
    }
  
</style>
{{-- <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-min-height-100% g-flex-middle">
        <div class="row no-gutters">
          <div class="col-md-6 order-md-1" style="height:100%; background:#070c17;">
            <div class="g-pa-15x">
              <div class="g-mb-100">
                <h1 class="g-color-white g-font-weight-700 g-font-size-50 g-letter-spacing-0_5 mb-4">We are Unify</h1>
                <p class="lead g-color-white">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key.</p>
              </div>
  
              <div class="row">
                <div class="col-md-7 col-lg-6 order-md-2 g-mb-50 g-mb-0--md">
                  <h2 class="h4 g-color-white-opacity-0_9 g-font-weight-700 mb-4">Contact us</h2>
                  <ul class="list-unstyled g-color-white-opacity-0_8 g-font-size-13 mb-0">
                    <li class="mb-3">
                      <i class="mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i> 795 Folsom Ave, Suite 600,
                      <br>San Francisco, CA 94107 795
                    </li>
                    <li class="mb-3"><i class="mt-1 mr-3 icon-communication-033 u-line-icon-pro"></i>(+123) 456 7890
                    </li>
                    <li class="mb-3"><i class="mt-1 mr-3 icon-communication-062 u-line-icon-pro"></i>info@htmlstream.com
                    </li>
                    <li><i class="mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i> www.htmlstream.com</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 order-md-2 g-color-black-opacity-0_8 g-bg-size-cover g-bg-pos-top-center g-min-height-400" data-bg-img-src="/menu1.jpg" style="background-image: url(/menu1.jpg);"></div>
        </div>
</section> --}}

{{-- <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-min-height-100% g-flex-middle" data-options="{direction: 'reverse';, settings_mode_oneelement_max_offset: '150';}">
        <div class="divimage dzsparallaxer--target w-100 g-bg-pos-bottom-center" style="height: 120%; background-image: url(/catering.jpeg); transform: translate3d(0px, -85.5069px, 0px);"></div>
  
        <div class="container g-py-100">
          <div class="row">
            <div class="col-md-6">
              <h3 style="font-size:40px" class="g-color-white Restaurant-Display tp-resizeme mb-4">
            OUR CATERING SERVICES
          </h3>
              <p class="Restaurant-Price p_description" >
                    Delhi ‘O’ Delhi provides an up-market, high quality and reliable catering service suitable for function of all types and sizes. We can help you create a unique menu, tailor made for the needs of you and your guests. We also give you the flexibility in choosing the best kind of services for your events, including buffets or table service delivered by our experienced team.

<br><br>
To top if all off, we can operate in all kind of venues, whether it be one of your choice or one of the major venues affiliated with us. We operate under the foremost principle that your special event should be remembered for years to come. For us this is a matter of integrity and trust. 
            </div>
          </div>
        </div>
</section> --}}

<section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall g-min-height-100% g-flex-middle">
        <div class="row no-gutters">
          <div class="col-md-6 order-md-2 g-bg-primary" style="height:100%">
            <div style="padding: 10% 10%">
              <div class="g-mb-20">
    
    <h3 style="font-size:40px; line-height:4rem;color:#2c2725 !important;" class="Restaurant-Cursive tp-resizeme mb-4">Our Catering Services</h3>
                <p class="Restaurant-Price p_description ">
                        The Delhi ‘O’ Delhi brand is now well-established as an event services partner in and around Sydney, including affiliations with five-star hotels in CBD and the most well-established function centers. Repeat business with our esteemed partners from a large portion of our catering services.

                </p>
                <p class="Restaurant-Price p_description ">
                        ” We now cater for weddings, family, event and all kind of social occasions and we are also increasingly asked to cater for large corporate events. The Delhi ‘O’ Delhi brand has grown more quickly than I ever anticipated – Javed Khan “
                </p>
                <br>
                <a class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link"  href="#modal1"  data-modal-target="#modal1" data-modal-effect="fadein">Inquire Now</a>
                <br><br>
                <button class="tp-caption rev-btn rev-bordered tp-withaction rs-hover-ready btn_link">Download Brochure</button>

            </div>
  
              
            </div>
          </div>
          <div class="col-md-6 order-md-1 g-color-black-opacity-0_8 g-bg-size-cover g-bg-pos-top-center g-min-height-400" data-bg-img-src="/catering.jpg" style="background-image: url(/catering.jpg);"></div>
        </div>
</section>



<!-- Demo modal window -->
<div id="modal1" class="text-left g-max-width-600 g-bg-primary g-overflow-y-auto g-pa-20" style="display: none; max-width:1000px !important">
  <button type="button" class="close" onclick="Custombox.modal.close();">
    <i class="hs-icon hs-icon-close"></i>
  </button>
  <h4 class="g-mb-20">Catering Inquiry</h4>
  <form>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group g-mb-20">
          <label class="g-mb-10 g-font-weight-600" for="inputGroup1_1">First Name</label>
          <input id="inputGroup1_1" class="form-control g-bg-primary form-control-md rounded-0" style="border:1px solid black" type="text" placeholder="Enter your first name.">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group g-mb-20">
          <label class="g-mb-10 g-font-weight-600" for="inputGroup1_1">Last Name</label>
          <input id="inputGroup1_1" class="form-control g-bg-primary form-control-md rounded-0" style="border:1px solid black" type="text" placeholder="Enter your last name.">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group g-mb-20">
          <label class="g-mb-10 g-font-weight-600" for="inputGroup1_1">Email</label>
          <input id="inputGroup1_1" class="form-control g-bg-primary form-control-md rounded-0" style="border:1px solid black" type="email" placeholder="Enter your email.">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group g-mb-20">
          <label class="g-mb-10 g-font-weight-600" for="inputGroup1_1">Contact No.</label>
          <input id="inputGroup1_1" class="form-control g-bg-primary form-control-md rounded-0" style="border:1px solid black" type="text" placeholder="Enter your phone number.">
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-md-12">
        <div class="form-group g-mb-20">
          <label class="g-mb-10 g-font-weight-600" for="inputGroup1_1">Message</label>
          <textarea id="inputGroup1_1" class="form-control g-bg-primary form-control-md rounded-0" style="border:1px solid black"  placeholder="Leave us message."></textarea>
        </div>
      </div>
    </div>
    <input type="submit" class="btn btn_grey pull-right"/>
  </form>  
</div>

@endsection


@section('js')
<!-- JS Plugins Init. -->
<script >
</script>
    <script  src="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.js"></script>

    <!-- JS Unify -->
    <script  src="/frontend-assets/main-assets/assets/js/components/hs.modal-window.js"></script>

    <!-- JS Plugins Init. -->
    <script >
      $(document).on('ready', function () {
        // initialization of popups
        $.HSCore.components.HSModalWindow.init('[data-modal-target]');
      });
    </script>
  
@endsection