 
   <!-- Header Toggle Button -->
  <style>
    .nav_link{
      padding: .5rem 1rem;
      font-size:22px;
      color:white ;
      line-height: 3.2rem;
    }
    .nav_link:hover{
      color:#eaa7a6;
      text-decoration: none;

    }
    .btn_link_nav{
            z-index: 999;
        white-space: nowrap;
        font-size: 17px;
        line-height: 17px;
        font-weight: 500;
        color: rgb(255, 255, 255);
        font-family: Roboto;
        text-transform: uppercase;
        background-color: #0a0a0a54;
        border: 2px solid rgba(255, 255, 255, 0.5);
        letter-spacing: 3px;
        }
      .btn_link_nav_m{
            z-index: 999;
        white-space: nowrap;
        font-size: 17px;
        line-height: 17px;
        font-weight: 500;
        color: rgb(255, 255, 255);
        font-family: Roboto;
        text-transform: uppercase;
        background-color: transparent;
        border: 2px solid rgba(255, 255, 255, 0.5);
        letter-spacing: 3px;
        }
        .btn_link:hover{
            border-color: #2b4e56
        }
      .float-right{
        position:fixed;
        right:18px;
        top:20px;
        z-index:99;
      }

    .hidden-sm{
      display:none;
    }
    .show-sm{
      display:inline;
    }

      @media only screen and (min-width: 600px) {
        .hidden-sm{
          display:none;
        }
        .show-sm{
          display:inline;
        }
    } 

    @media only screen and (min-width: 768px) {
      .hidden-sm{
        display:inline;
      }
      .show-sm{
          display:none;
      }
    } 

    @media only screen and (min-width: 1000px) {
      .hidden-sm{
        display:inline;
      }
      .show-sm{
          display:none;
      }
    } 
    
   
  </style>
  <div class="hidden-sm float-right">
  <button class=" btn btn_link_nav pull-right">Make reservation</button>
  <button style="margin-right:1rem" class="btn btn_link_nav pull-right">Order Online</button>
  </div>

   <button style="left:1rem" class="btn u-btn-white u-header-toggler u-header-toggler--top-right g-pa-0" id="header-toggler" aria-haspopup="true" aria-expanded="false" aria-controls="js-header" aria-label="Toggle Header" data-target="#js-header">
    <span class="hamburger hamburger--collapse">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
    </span>
    </span>
  </button>
  <!-- End Header Toggle Button -->
{{-- #d90001 --}}
  <!-- Sidebar Navigation -->
  <div id="js-header" class="u-header u-header--side" aria-labelledby="header-toggler" data-header-behavior="overlay" data-header-position="left" data-header-breakpoint="lg" data-header-classes="g-transition-0_5" data-header-overlay-classes="g-bg-black-opacity-0_8 g-transition-0_5">
    <div style="background:  #b32826" class="u-header__sections-container g-brd-right--lg g-brd-gray-light-v5 g-py-10 g-py-40--lg g-px-14--lg">
      <div class="u-header__section u-header__section--light">
        <nav class="navbar navbar-expand-lg">
          <div class="js-mega-menu container">
            <!-- Responsive Toggle Button -->
           <div class="row" style="width:100%">
             <div class="col-sm-3 col-lg-12" style="width:20%">
            <button  class=" navbar-toggler u-btn-white navbar-toggler-left btn g-line-height-1 g-brd-none g-pa-0  g-left-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
            </button>
          </div>
            <!-- End Responsive Toggle Button -->
            <!-- Logo -->
            <div class="col-lg-12 text-center hidden-sm">
            <a href="/" class="navbar-brand g-mb-40--lg g-mt-40">
              <img src="/logo.jpg" style="height:4rem" alt="Image Description">
            </a>
          </div>
            <div class="col-sm-9 show-sm" style="width:80%">
              
            <a href="/" class="navbar-brand g-mb-40--lg  pull-right " >
              <img src="/logo.jpg" style="height:4rem" alt="Image Description">
            </a>
            </div>
            
            <!-- End Logo -->
          </div>
            <!-- Navigation -->

            <div class="collapse navbar-collapse align-items-center flex-sm-row w-100 g-mt-20 g-mt-0--lg g-mb-40" id="navBar">
              <ul class="navbar-nav ml-auto text-uppercase g-font-weight-600 u-sub-menu-v1 text-center">
                <li class="nav-item g-my-5 active">
                  <a href="/"  class=" nav_link">This is Us
                
                  </a>
                  
                </li>
                <li class="nav-item g-my-5">
                  <a href="/menu" class="nav_link">Menu
                
                </a>
                </li>
                <li class="nav-item g-my-5">
                  <a href="/catering" class=" nav_link">Catering
                
              </a>
                </li>
                <li class="nav-item g-my-5">
                  <a href="/events"  class="nav_link">Upcoming Events
                
              </a>
                </li>
                
              </ul>
              <br>
              <div class="d-flex justify-content-center">
                <button class="show-sm btn btn_link_nav_m pull-right">Make reservation</button>
              </div>
              <br>
              <div class="d-flex justify-content-center">
                <button class="show-sm btn btn_link_nav_m pull-right">Order Online</button>
              </div>
            </div>
            <!-- End Navigation -->

            
          </div>
          
        </nav>
        
      </div>
    </div>
  </div>
  <!-- End Sidebar Navigation -->