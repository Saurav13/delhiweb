<!DOCTYPE html>
<html lang="en" class="font-primary">
  <head>
    <title>Delhi O Delhi </title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Fav -->
    <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%7CNothing+You+Could+Do%3A400%7CRoboto%3A300%2C700%7CRaleway%3A400%2C500%2C800">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">

  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line/css/simple-line-icons.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-etlinefont/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-line-pro/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-hs/style.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-etlinefont/style.css">

    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/layers.css">
    <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/css/navigation.css">
  
  
  
  </head>
