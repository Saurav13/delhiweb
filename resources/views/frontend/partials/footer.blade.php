@if(Request::segment(1)!='')

<style>
  footer a{
    color:#2c2725 !important;
    font-size:16px !important;
  }
  footer h3{
     color:#2c2725 !important;
    font-size:16px !important; 
    line-height:16px;
  }
  footer h2{
    color:#2c2725 !important;
    font-size:16px !important; 
    line-height:16px;
  }
  footer span{
    color:#2c2725 !important;
    
  }
  .btn_grey{
    color:#e2e2e2 !important;
    background: #947e7e;
  }
  .btn_grey:hover{
     background: #ab9696; 
  }
</style>
<footer>
    <section class="g-color-white g-pa-40" style="background:#fbdbcc">
        <div class="container">
          <div class="row">
            <div class="col-md-8 align-self-center">
              <h2  class="h3 Restaurant-Price text-uppercase g-font-weight-300 g-mb-20 g-mb-0--md">Learn More about our
                <strong>Gift Voucher</strong></h2>
            </div>
      
            <div class="col-md-4 align-self-center">
              <div class="g-brd-white--focus">
                <a href="#!" class="btn btn_grey pull-right">Gift Voucher</a>
              </div>
            </div>
          </div>
        </div>
      </section>
    <div style="background:#070d18">
      <div class="container g-pt-50 g-pb-20">
        <div class="row">
          <div class="col-6 col-md-3 g-mb-30">
            <h3 class="Restaurant-Price h6 g-color-white text-uppercase">SiteMap</h3>
  
            <!-- Links -->
            <ul class="list-unstyled mb-0">
              <li>
                <a  class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="page-about-1.html">
                  About
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a   class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="page-services-1.html">
                 Gift Voucher
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a style="font-size:18px"  class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="page-pricing-1.html">
                  Pricing Tables
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a  class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="page-hire-us-1.html">
                  Hire Unify Marketing
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="page-apply-1.html">
                  Apply for a Job
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a  class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="page-contacts-1.html">
                  Contacts
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
            </ul>
            <!-- End Links -->
          </div>
  
          <div class="col-6 col-md-3 g-mb-30">
            <h3 style="font-size:18px; line-height:20px" class="Restaurant-Price h6 g-color-white text-uppercase">Insiders</h3>
  
            <!-- Links -->
            <ul class="list-unstyled mb-0">
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span>Insider Information</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span>Information for Insiders</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span>Personal Information</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span>Enterprise Information</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
            </ul>
            <!-- End Links -->
          </div>
  
          <div class="col-6 col-md-3 g-mb-30">
            <h3 style="font-size:18px; line-height:20px" class="Restaurant-Price h6 g-color-white text-uppercase">Delhi O delhi Special</h3>
  
            <!-- Links -->
            <ul class="list-unstyled mb-0">
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span>Online</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span> Express</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span> Finance</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span> Business</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
              <li>
                <a class="Restaurant-Price u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span > Ads</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
            </ul>
            <!-- End Links -->
          </div>
  
          <div class="col-6 col-md-3 g-mb-30">
            <!-- Links -->
            <ul class="list-inline">
              <li class="list-inline-item mb-1">
                <a class="u-icon-v2 u-icon-size--xs g-brd-gray-light-v2 g-brd-primary--hover g-color-gray-light-v1 g-color-primary--hover rounded" target="_blank" href="https://www.facebook.com/delhiodelhi">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item mb-1">
                <a class="u-icon-v2 u-icon-size--xs g-brd-gray-light-v2 g-brd-primary--hover g-color-gray-light-v1 g-color-primary--hover rounded" target="_blank" href="https://www.instagram.com/delhiodelhi">
                  <i class="fa fa-instagram"></i>
                </a>
              </li>
              {{-- <li class="list-inline-item mb-1">
                <a class="u-icon-v2 u-icon-size--xs g-brd-gray-light-v2 g-brd-primary--hover g-color-gray-light-v1 g-color-primary--hover rounded" href="#">
                  <i class="fa fa-windows"></i>
                </a>
              </li> --}}
            </ul>
  
            {{-- <ul class="list-unstyled mb-4">
              <li>
                <a class="u-link-v6 g-color-gray-dark-v5 g-color-primary--hover g-font-weight-500 g-text-underline--none--hover g-py-3" href="#">
                  <span>How to Install Apps</span>
                  <span class="u-link-v6-arrow g-font-size-18">→</span>
                </a>
              </li>
            </ul> --}}
            <!-- End Links -->
  
            <h3 class="h6 g-color-white text-uppercase">Address</h3>
            <address class="g-font-size-13 g-color-gray-dark-v3 g-font-weight-600 g-line-height-2 mb-0">3 Erskinevilla Road,<br>New Town,<br> NSW 2042, Australia</address>
          </div>
        </div>
      </div>
    </div>
  
    {{-- <div class="g-bg-gray-dark-v1">
      <div class="container g-pt-30">
        <div class="row align-items-center">
          <div class="col-md-4 text-center text-md-left g-mb-30">
            <!-- Logo -->
            <a class="g-text-underline--none--hover mr-4" href="index.html">
              <img class="g-width-95" src="../../../assets/img/logo/logo-2.png" alt="Logo">
            </a>
            <!-- End Logo -->
            <p class="d-inline-block align-middle g-color-gray-dark-v5 g-font-size-13 mb-0">© 2019 Htmlstream.
              <br>All Rights Reserved.</p>
          </div>
  
          <div class="col-md-4 g-mb-30">
            <!-- Social Icons -->
            <ul class="list-inline text-center mb-0">
              <li class="list-inline-item">
                <a class="u-icon-v3 u-icon-size--sm g-color-gray-light-v1 g-color-white--hover g-bg-transparent g-bg-primary--hover rounded" href="#"><i class="fa fa-facebook"></i></a>
              </li>
              <li class="list-inline-item">
                <a class="u-icon-v3 u-icon-size--sm g-color-gray-light-v1 g-color-white--hover g-bg-transparent g-bg-primary--hover rounded" href="#"><i class="fa fa-twitter"></i></a>
              </li>
              <li class="list-inline-item">
                <a class="u-icon-v3 u-icon-size--sm g-color-gray-light-v1 g-color-white--hover g-bg-transparent g-bg-primary--hover rounded" href="#"><i class="fa fa-pinterest"></i></a>
              </li>
              <li class="list-inline-item">
                <a class="u-icon-v3 u-icon-size--sm g-color-gray-light-v1 g-color-white--hover g-bg-transparent g-bg-primary--hover rounded" href="#"><i class="fa fa-instagram"></i></a>
              </li>
              <li class="list-inline-item">
                <a class="u-icon-v3 u-icon-size--sm g-color-gray-light-v1 g-color-white--hover g-bg-transparent g-bg-primary--hover rounded" href="#"><i class="fa fa-youtube"></i></a>
              </li>
            </ul>
            <!-- End Social Icons -->
          </div>
  
          <div class="col-md-4 g-mb-30 text-right">
            <div class="d-inline-block g-mx-15">
              <h4 class="g-color-gray-dark-v5 g-font-size-11 text-left text-uppercase">Email</h4>
              <a href="#">unify@marketing.com</a>
            </div>
            <div class="d-inline-block g-mx-15">
              <h4 class="g-color-gray-dark-v5 g-font-size-11 text-left text-uppercase">Phone</h4>
              <a href="#">(0161) 347 8854</a>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
</footer>
  <!-- End Footer -->
@endif

<!-- JS Global Compulsory -->
<script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery.easing/js/jquery.easing.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/popper.js/index.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/tooltip.js/index.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

<script src="/frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/masonry/dist/masonry.pkgd.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/gmaps/gmaps.min.js"></script>

<!-- JS Unify -->
<script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>

<script src="/frontend-assets/main-assets/assets/js/components/hs.header-side.js"></script>
<script src="/frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js"></script>

<script src="/frontend-assets/main-assets/assets/js/components/hs.dropdown.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.scrollbar.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.popup.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.carousel.js"></script>

<script src="/frontend-assets/main-assets/assets/js/components/gmap/hs.map.js"></script>

<script src="/frontend-assets/main-assets/assets/js/components/hs.go-to.js"></script>

<!-- JS Revolution Slider -->
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>


@yield('js')
<!-- JS Custom -->
<script src="/frontend-assets/main-assets/assets/js/custom.js"></script>

<!-- JS Plugins Init. -->
<script>
  $(document).ready(function(){
    $(window).on('load', function () {
      // initialization of header
      $.HSCore.components.HSHeaderSide.init($('#js-header'));
      $.HSCore.helpers.HSHamburgers.init('.hamburger');

      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        direction: 'vertical',
        breakpoint: 991
      });
  });
  });

  </script>
<script>

  
  var tpj = jQuery,
    revAPI;

  tpj(document).ready(function () {
    if (tpj('#revSlider').revolution == undefined) {
      revslider_showDoubleJqueryError('#revSlider');
    } else {
      revAPI = tpj('#revSlider').show().revolution({
        sliderType: 'standard',
        jsFileLocation: 'revolution/js/',
        sliderLayout: 'fullscreen',
        dottedOverlay: 'none',
        delay: 9000,
        navigation: {
          keyboardNavigation: 'off',
          keyboard_direction: 'horizontal',
          mouseScrollNavigation: 'on',
          mouseScrollReverse: 'default',
          onHoverStop: 'off',
          touch: {
            touchenabled: 'on',
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: 'vertical',
            drag_block_vertical: false
          },
          // bullets: {
          //   enable: true,
          //   hide_onmobile: true,
          //   hide_under: 1024,
          //   style: 'uranus',
          //   hide_onleave: false,
          //   direction: 'vertical',
          //   h_align: 'right',
          //   v_align: 'center',
          //   h_offset: 30,
          //   v_offset: 0,
          //   space: 5,
          //   tmp: '<span class="tp-bullet-inner"></span>'
          // }
        },
        viewPort: {
          enable: true,
          outof: 'wait',
          visible_area: '80%',
          presize: false
        },
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1240, 1024, 778, 480],
        gridheight: [868, 768, 960, 720],
        lazyType: 'single',
        shadow: 0,
        spinner: 'off',
        stopLoop: 'on',
        stopAfterLoops: 0,
        stopAtSlide: 1,
        shuffle: 'off',
        autoHeight: 'off',
        fullScreenAutoWidth: 'off',
        fullScreenAlignForce: 'off',
        fullScreenOffsetContainer: '.header',
        disableProgressBar: 'on',
        hideThumbsOnMobile: 'off',
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
          simplifyAll: 'off',
          nextSlideOnWindowFocus: 'off',
          disableFocusListener: false
        }
      });
    }
  });
</script>
</html>
